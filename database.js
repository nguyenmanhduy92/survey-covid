'use strict'

const env = process.env.NODE_ENV || 'development'
const knexfile = require('./config/knexfile')
const knex_connection = require('knex')(knexfile[env])

module.exports = knex_connection