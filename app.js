const express = require('express')
const app = express()

const port = process.env.PORT || '8000'
const path = require('path')
const bodyParser = require('body-parser')
const db = require('./config/mysql.config')
global.db = db;

// configure middleware
app.set('port', port);
app.set('views', './views')
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json()); // parse form data client
app.use(express.static(path.join(__dirname, 'public')))

// Routes
app.get('/bookings', (request, response) => {
  let query = "SELECT * FROM `booking` ORDER BY id ASC";

  db.query(query, (err, results) => {
    if (err) {
      response.redirect("google.com");
    }

    response.render("bookings/index", {bookings: results})
  })
});

app.get("/", (request, response) => {
  response.render('new')
})
/*
app.get('/', getHomePage);

app.get('/add', addPlayerPage);
app.get('/edit/:id', editPlayerPage);
app.get('/delete/:id', deletePlayer);
app.post('/add', addPlayer);
app.post('/edit/:id', editPlayer);
*/

// set the app to listen on the port
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
});
