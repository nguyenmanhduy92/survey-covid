const db_configs = require('./db.config')
const mysql = require('mysql')
const db = mysql.createConnection({
  host: db_configs['development']['HOST'],
  user: db_configs['development']['USER'],
  password: db_configs['development']['PASSWORD'],
  database: db_configs['development']['DATABASE'],
  port: db_configs['development']['PORT']
})

db.connect((err) => {
  if (err) {
    throw err;
  }
  console.log('Connect to database');
});

module.exports = db