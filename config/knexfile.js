const db_configs = require('./db.config.js')
const db_development = db_configs['development']
const db_staging = db_configs['staging']
const db_production = db_configs['production']

const db_connection = {
  development: {
    client: db_development['CLIENT'],
    connection: {
      host: db_development['HOST'],
      user: db_development['USER'],
      port: db_development['PORT'],
      password: db_development['PASSWORD'],
      database: db_development['DATABASE'],
    },
    debug: true
  },
  staging: {
    client: db_staging['CLIENT'],
    connection: {
      host: db_staging['HOST'],
      user: db_staging['USER'],
      port: db_staging['PORT'],
      password: db_staging['PASSWORD'],
      database: db_staging['DATABASE']
    }
  },
  production: {
    client: db_production['CLIENT'],
    connection: {
      host: db_production['HOST'],
      user: db_production['USER'],
      port: db_production['PORT'],
      password: db_production['PASSWORD'],
      database: db_production['DATABASE']
    }
  }
}
module.exports = db_connection